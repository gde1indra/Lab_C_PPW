from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Gde Indra Raditya Nugraha' # TODO Implement this
#mhs_name1 = 'I Made Adisurya Nugraha'
#mhs_name2 = 'I Kade Hendrick Putra Kurniawan'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,2,7) #TODO Implement this, format (Year, Month, Date)
#birth_date1 = date(1999,3,10)
#birth_date2 = date(1999, 3, 11)
npm = 1706984606 # TODO Implement this
#npm1 = 1706984625
#npm2 = 1706984612
college = "University of Indonesia"
hobby = "Basketball"
#hobby1 = "Bermain musik, berolahraga, belajar"
#hobby2 = "Editing"
description = "A Balinese people,who loves sport and gaming"
#description1 = "Seorang mahasiswa Fasilkom UI 2017 yang ingin belajar PPW"
#description2 ="I am proficient at communicating, building, and maintaining profesional relationships. Have good experience in leaderships, visual design, and cinematography"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year),'npm': npm,'college':college,'hobby':hobby,'description':description}
    return render(request, 'challenge_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
